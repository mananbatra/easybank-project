document.addEventListener("DOMContentLoaded", function() {
    var menuButton = document.getElementById("menuButton");
    var menuOverlay = document.getElementById("menuOverlay");
  
    menuButton.addEventListener("click", function() {

      if( menuOverlay.style.display === "none")  
      {
          menuOverlay.style.display = "block";
          menuButton.innerHTML=`<img src="./images/icon-close.svg">`;
      }
      else{
        menuOverlay.style.display = "none";
        menuButton.innerHTML=`<img src="./images/icon-hamburger.svg">`
      }

    });
  
    menuOverlay.addEventListener("click", function(e) {
      if (e.target === menuOverlay) {
        menuOverlay.style.display = "none";
      }
    });
  });
  